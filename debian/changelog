tomatoes (1.55-10) unstable; urgency=medium

  * Bump DH to 13
  * Bump Standards-Version to 4.5.1
  * Add d/gbp.conf
  * Add debian/salsa-ci.yml
  * d/control: Add Rules-Requires-Root: no

 -- Samuel Henrique <samueloph@debian.org>  Fri, 29 Jan 2021 19:43:28 +0000

tomatoes (1.55-9) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/watch: Use https protocol

  [ Helmut Grohne ]
  * Fix FTCBFS: Supply C++ compiler via CC. (Closes: #918021)

  [ Samuel Henrique ]
  * Bump DH level to 12
  * Bump Standards-Version to 4.3.0

 -- Samuel Henrique <samueloph@debian.org>  Wed, 02 Jan 2019 21:23:57 +0000

tomatoes (1.55-8) unstable; urgency=medium

  * Bump DH level to 11
  * Bump Standards-Version to 4.2.1
  * Update Vcs-* fields to salsa
  * Update my email to @debian.org
  * d/control: Set tomatoes-data as Multi-Arch: foreign

 -- Samuel Henrique <samueloph@debian.org>  Wed, 26 Sep 2018 21:37:12 -0300

tomatoes (1.55-7) unstable; urgency=medium

  * Bump DH to 10
  * Bump Standards-Version to 4.1.0
  * d/control: add Vcs-Git fields to collab-maint
  * d/copyright: update years
  * d/patches/05_files: fix file locations, patch from the Arch Linux
    package, made by Michael Straube <straubem@gmx.de> (closes: #875832)
  * d/rules: use pkg-info.mk to get package version
  * wrap-and-sort -a

 -- Samuel Henrique <samueloph@debian.org>  Tue, 26 Sep 2017 19:00:42 -0300

tomatoes (1.55-6) unstable; urgency=medium

  * New maintainer, thanks to all the previous people that worked on this
    package, i also would like to mention Joao Eriberto Mota Filho
    <eriberto@debian.org>, whom sponsored me (closes: #833464).
  * debian/control:
    - Bump Standards-Version to 3.9.8.
    - [tomatoes|tomatoes-data]: Description: Better alignment.
    - tomatoes-data: Change description to mention music files.
    - tomatoes-data: Remove ${shlibs:Depends} dependency.
  * debian/copyright: Convert to dep-5 format.
  * debian/install: Remove debian/menuicon.xpm.
  * debian/menuicon.xpm: Remove, it was only needed for debian/tomatoes.menu.
  * debian/patches:
    - Add missing header to all patches.
    - flags.diff: Merge into 08_flags.diff.
    - 04_credits.diff: Change to reflect new maintainer.
    - 08_flags.diff: Add CPPFLAGS and merge with flags.diff.
    - 09_sort_source_files.diff: Reproducible build, thanks to
      Reiner Herrmann (closes: #792529).
  * debian/rules:
    - Don't append date on version.
    - Export hardening=+all and remove trailing whitespace.
    - override_dh_installchangelogs: Generate and install changelog.
  * debian/tomatoes.6:
    - Add new upstream's email.
    - Fix typo "tomates".
    - Remove old mention of upstream's occupation.
  * debian/tomatoes.desktop:
    - Add Keywords.
    - Remove deprecated Encoding key.
  * debian/tomatoes.menu: Remove in favor of tomatoes.desktop.
  * debian/[tomatoes|tomatoes-data].lintian-overrides: Create override for
    "using-first-person-in-description", as description starts with the full
    name of the game: "I Have No Tomatoes".
  * debian/watch: Bump to v4.

 -- Samuel Henrique <samueloph@debian.org>  Sat, 20 Aug 2016 21:21:54 -0300

tomatoes (1.55-5) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format, debhelper 9, no cdbs.
  * debian/README.Debian-source: Moved into debian/copyright.
  * debian/tomatoes.6, debian/manpages: Added.  Closes: #626934.
  * debian/tomatoes.desktop: Updated.  Closes: #626933.
  * debian/patches/flags.diff: Added.

 -- Bart Martens <bartm@debian.org>  Wed, 23 May 2012 05:13:05 +0000

tomatoes (1.55-4) unstable; urgency=low

  * debian/patches/07_nosound.diff: Added.  Closes: #449331.

 -- Bart Martens <bartm@debian.org>  Fri, 02 Jan 2009 23:45:49 +0100

tomatoes (1.55-3) unstable; urgency=low

  * debian/patches/06_SDLK_KP_ENTER.diff: Added.
  * debian/tomatoes.desktop: Updated categories.
  * debian/tomatoes.menu: Updated section.

 -- Bart Martens <bartm@debian.org>  Sun, 14 Oct 2007 20:57:24 +0200

tomatoes (1.55-2) unstable; urgency=low

  * debian/patches/05_configfile.diff: Added.  Closes: #414678.  Patch by
    Enrico Zini <enrico@debian.org>, thanks.

 -- Bart Martens <bartm@knars.be>  Sat, 31 Mar 2007 17:49:48 +0200

tomatoes (1.55-1) unstable; urgency=low

  * Initial release.  Closes: #408985.

 -- Bart Martens <bartm@knars.be>  Mon, 29 Jan 2007 10:56:08 +0100
